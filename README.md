# Implementar um sistema de anúncios utilizando Java 8, Elasticsearch 2.0 e Java EE 7 #

Este artigo é útil por apresentar o desenvolvimento de um sistema de anúncios utilizando recursos de Java 8, armazenamento de dados distribuídos e consulta de dados baseada em semelhança de texto.

O sistema será desenvolvido utilizando o padrão de camadas MVC (Model View Control). A camada Model será implementada utilizando a especificação Java EE 7 com o banco  Elasticsearch, a camada Control utilizará AngularJS e a camada View utilizará HTML.

O servidor WildFly será adotado como implementação da especificação Java EE 7, o projeto irá utilizar CDI (JSR-346) para injeção de dependências, EJB (JSR-345) para armazenar as regras de negócio e JAX-RS (JSR-339) para expor os recursos através de serviços REST (Representational State Transfer).
