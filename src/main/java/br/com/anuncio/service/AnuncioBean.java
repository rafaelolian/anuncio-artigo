package br.com.anuncio.service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.anuncio.domain.Anuncio;
import br.com.anuncio.repository.AnuncioRepository;

@Stateless
public class AnuncioBean {
	
	@Inject
	private AnuncioRepository anuncioRepository;
	
	public void cadastrar( Anuncio anuncio, UUID idAnunciante ){
		anuncio.setId(UUID.randomUUID());
		anuncio.setIdAnunciante(idAnunciante);
		anuncio.setDataCadastro(LocalDateTime.now());
		anuncioRepository.salvar(anuncio);
	}

	public void atualizar( Anuncio anuncio, UUID idAnunciante ){
		anuncio.setIdAnunciante(idAnunciante);
		anuncio.setDataAtualizacao(LocalDateTime.now());
		anuncioRepository.atualizar(anuncio);
	}

	public Collection<Anuncio> pesquisarPorSemelhanca( String texto ){
		return anuncioRepository.pesquisarPorSemelhanca(texto);
	}

	public Anuncio buscarPorId( UUID id ){
		return anuncioRepository.buscarPorId(id);
	}	
	
	public void excluir( UUID id ){
		anuncioRepository.excluir(id);
	}
}