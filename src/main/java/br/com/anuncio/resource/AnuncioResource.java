package br.com.anuncio.resource;

import java.util.Collection;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import br.com.anuncio.domain.Anuncio;
import br.com.anuncio.service.AnuncioBean;

@Path("anuncios")
public class AnuncioResource 
{
	@Inject
	private AnuncioBean anuncioBean;
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Anuncio cadastrar(Anuncio anuncio) {
		anuncioBean.cadastrar(anuncio, AnuncianteResource.getIdAnuncianteAutenticado());
		return anuncio;
	}

	@PUT
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Anuncio atualizar(@PathParam("id") UUID id, Anuncio anuncio) {
		anuncio.setId(id);
		anuncioBean.atualizar(anuncio, AnuncianteResource.getIdAnuncianteAutenticado());
		return anuncio;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Anuncio> pesquisar(@QueryParam("pesquisa") String pesquisa) {
		return anuncioBean.pesquisarPorSemelhanca(pesquisa);
	}

	@GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Anuncio buscarPorId(@PathParam("id") UUID id) {
        return anuncioBean.buscarPorId(id);
    }

    @DELETE
    @Path("{id}")
    public void excluir(@PathParam("id") UUID id) {
    	anuncioBean.excluir(id);
    }
}