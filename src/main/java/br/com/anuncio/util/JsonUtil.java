package br.com.anuncio.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.anuncio.domain.Anuncio;

public class JsonUtil {

	private static final ObjectMapper MAPPER = new ObjectMapper();
	
	static {
		MAPPER.findAndRegisterModules();
	}
	
	public static String toJson(Anuncio anuncio) {
		try {
			return MAPPER.writeValueAsString(anuncio);
		} catch (JsonProcessingException e) {
			throw new IllegalArgumentException(e);
		}
	}
	
	public static <T> T fromJson( String json, Class<T> type ){
		try {
			return MAPPER.readValue(json, type);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}
}