package br.com.anuncio.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public class Anuncio {
	
	private UUID id;
	
	private String titulo;
	
	private String descricao;
	
	private String categoria;
	
	private String estado;
	
	private String cidade;
	
	private UUID idAnunciante;
	
	private BigDecimal valor;
	
	private LocalDateTime dataCadastro;
	
	private LocalDateTime dataAtualizacao;

	public UUID getId() {
		return id;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getCategoria() {
		return categoria;
	}

	public String getEstado() {
		return estado;
	}

	public String getCidade() {
		return cidade;
	}

	public UUID getIdAnunciante() {
		return idAnunciante;
	}

	public void setIdAnunciante( UUID idAnunciante ) {
		this.idAnunciante = idAnunciante;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public LocalDateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(LocalDateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

}
