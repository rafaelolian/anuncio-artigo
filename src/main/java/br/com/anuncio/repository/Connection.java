package br.com.anuncio.repository;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

@Singleton
@Startup
public class Connection {

	private Client client;

	@PostConstruct
	private void startup() throws UnknownHostException {
		Settings settings = Settings.settingsBuilder().put("cluster.name", "artigo").build();
		client = TransportClient.builder().settings(settings).build()
				.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"),9300));
	}

	@PreDestroy
	private void shutdown() {
		client.close();
	}
	
	@Produces
	public Client getClient(){
		return client;
	}
}