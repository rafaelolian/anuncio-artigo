package br.com.anuncio.repository;

import static br.com.anuncio.util.JsonUtil.fromJson;
import static br.com.anuncio.util.JsonUtil.toJson;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import javax.inject.Inject;

import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;

import br.com.anuncio.domain.Anuncio;

public class AnuncioRepository {
	
	private static final String INDICE = "artigo";
	private static final String TIPO = "anuncio";
	
	@Inject
	private Client client;
	
	public void salvar( Anuncio anuncio ){
		client.prepareIndex(INDICE, TIPO, anuncio.getId().toString()).setSource(toJson(anuncio)).get();
	}
	
	public void atualizar( Anuncio anuncio ){
		UpdateRequest updateRequest = new UpdateRequest(INDICE, TIPO, anuncio.getId().toString()); 
		updateRequest.doc(toJson(anuncio));
		client.update(updateRequest);
	}
	
	public Collection<Anuncio> pesquisarPorSemelhanca( String texto ){
		SearchResponse response = client.prepareSearch(INDICE).setTypes(TIPO)
			.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
			.setQuery(QueryBuilders.multiMatchQuery(texto, "titulo", "descricao", "categoria", "estado", "cidade"))
			.get();
		
		Collection<Anuncio> anuncios = new ArrayList<Anuncio>();
		
		response.getHits().forEach( hit ->
			anuncios.add( fromJson(hit.getSourceAsString(), Anuncio.class) )
		);
		
		return anuncios;
	}
		
	public Anuncio buscarPorId(UUID id) {
		GetResponse response = client.prepareGet(INDICE, TIPO, id.toString()).get();
		return fromJson(response.getSourceAsString(), Anuncio.class);
	}
	
	public void excluir( UUID id ){
		client.prepareDelete(INDICE, TIPO, id.toString());
	}	
}