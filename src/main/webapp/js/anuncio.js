function listagemController($scope, $http) {
    $scope.pesquisaLivre = function() {
		var res = $http.get('api/anuncios?pesquisa='+ $scope.pesquisa);
		res.success(function(data, status, headers, config) {
			$scope.anuncios = data;
		});
		res.error(function(data, status, headers, config) {
			$scope.mensagem = "Erro ao realizar pesquisa";
		});	        	
    }    
}

function cadastroController($scope, $http) {
    $scope.cadastrar = function() {
		var res = $http.post('api/anuncios', $scope.anuncio);
		res.success(function(data, status, headers, config) {
    		$scope.mensagem = "Cadastro realizado com sucesso";
		});
		res.error(function(data, status, headers, config) {
			$scope.mensagem = "Erro ao cadastrar anúncio";
		});	        	
    }
    
    $http.get('js/estados.json').success(function(data) {
        $scope.estados = data;
    });
}